import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.15

Pane {
    ColumnLayout {
        Layout.fillWidth: true

        Text {
            text: "Please Connect to Plex Server"
            font.pointSize: 30
            bottomPadding: 20
            color: "#fff"
            Layout.alignment: Qt.AlignHCenter
        }

        Button {
            text: qsTr("Connect")
            highlighted: true
            Material.background: Material.color(Material.Green, Material.Shade600)
            onClicked: connectModal.open()
            font.pointSize: 18
            Layout.alignment: Qt.AlignHCenter
        }
    }

    Popup {
        id: connectModal
        anchors.centerIn: Overlay.overlay
        padding: 20

        width: 700
        height: 500

        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        GridLayout {
            id: connectFormLayout
            rows: 7
            columns: 3
            rowSpacing: -50
            anchors {
                fill: parent
                leftMargin: 100
                rightMargin: 100
            }

            Text {
                id: connectModalTitle
                text: "Connect to Server"
                font.pointSize: 30
                color: "#fff"
                Layout.columnSpan: 3
                Layout.alignment: Qt.AlignCenter
            }

            Label {
                text: "Protocol:"
                Layout.columnSpan: 1
                Layout.fillWidth: true
            }
            ComboBox {
                model: ["smb", "http", "https"]
                Layout.columnSpan: 2
                Layout.fillWidth: true
            }

            Label {
                text: "IP Address:"
                Layout.columnSpan: 1
                Layout.fillWidth: true
            }
            TextField {
                placeholderText: qsTr("XX.XX.XX.XX")
                Layout.columnSpan: 2
                Layout.fillWidth: true
            }

            Label {
                text: "Port:"
                Layout.columnSpan: 1
                Layout.fillWidth: true
            }
            TextField {
                placeholderText: qsTr("XXX")
                Layout.columnSpan: 2
                Layout.fillWidth: true
            }

            Label {
                text: "Username:"
                Layout.columnSpan: 1
                Layout.fillWidth: true
            }
            TextField {
                placeholderText: qsTr("Username...")
                Layout.columnSpan: 2
                Layout.fillWidth: true
            }

            Label {
                text: "Password:"
                Layout.columnSpan: 1
                Layout.fillWidth: true
            }
            TextField {
                placeholderText: qsTr("Password...")
                Layout.columnSpan: 2
                Layout.fillWidth: true
            }

            Button {
                text: qsTr("Connect")
                Material.background: Material.color(Material.Green, Material.Shade600)
                onClicked: connectModal.close()
                Layout.columnSpan: 3
                Layout.alignment: Qt.AlignRight
            }
        }
    }
}
