import QtQuick 2.12
import QtQuick.Controls 2.5

// NOTE: Qt.Quick.Dialog can be used for file selection

ApplicationWindow {
    id: window
    width: maximumHeight
    height: maximumWidth
    visible: true
    title: qsTr("Plex Uploader")

    Text {
        id: title
        text: "Plex Uploader"
        font.pointSize: 46
        color: "#fff"
        padding: 50
        anchors {
            horizontalCenter: parent.horizontalCenter
        }
    }

    Connect {
        topPadding: 250
        anchors {
            top: title.bottom
            horizontalCenter: parent.horizontalCenter
        }
    }

}
